import java.util.ArrayList;
import java.util.Date;
import com.devcamp.j03_javabasic.s50.Order; //inport class Order

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Order> arrayList = new ArrayList<>();
        //khởi tạo order với các tham số khác nhau (Khởi tại 4 object Order)
        Order Order1 = new Order();
        Order Order2 = new Order("Lan");
        Order Order3 = new Order (3, "Long", 80000);
        Order Order4 = new Order(4, "Nam", 75000, new Date(), false, new String[] {"hop mau", "tay", "giay mau"});
        //thêm objcect order vào danh sách
        arrayList.add(Order1);
        arrayList.add(Order2);
        arrayList.add(Order3);
        arrayList.add(Order4);

        //in ra màn hình
        for (Order order:arrayList){
            System.out.println(order.toString());
        }

    }
}
